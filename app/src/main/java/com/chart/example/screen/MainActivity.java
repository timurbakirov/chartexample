package com.chart.example.screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.TextView;

import com.chart.example.util.DateUtils;
import com.chart.example.R;
import com.chart.example.model.ChartItem;
import com.chart.example.socket.SocketManager;
import com.chart.example.util.TextUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BubbleMarkerView.MarkerListener, SocketManager.TickerCallback {

    private LineChart mChart;

    private List<ChartItem> mChartItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mChart = findViewById(R.id.line_chart);

        mChart.setPinchZoom(true);
        mChart.getDescription().setEnabled(false);
        mChart.getAxisLeft().setEnabled(false);
        mChart.getLegend().setEnabled(false);
        mChart.setExtraLeftOffset(20f);

        BubbleMarkerView mv = new BubbleMarkerView(this, R.layout.view_marker, this);
        mv.setChartView(mChart);
        mChart.setMarker(mv);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextSize(10f);
        xAxis.setTextColor(ContextCompat.getColor(this, R.color.chart_axis_text));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularityEnabled(true);
        xAxis.setGridColor(ContextCompat.getColor(this, R.color.chart_axis_grid));
        xAxis.setGridLineWidth(1f);
        xAxis.setDrawAxisLine(false);
        xAxis.setLabelCount(4);
        xAxis.setValueFormatter(new XAxisValueFormatter());

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setTextSize(10f);
        rightAxis.setTextColor(ContextCompat.getColor(this, R.color.chart_axis_text));
        rightAxis.setDrawGridLines(true);
        rightAxis.setDrawAxisLine(false);
        rightAxis.setGranularityEnabled(true);
        rightAxis.setGridColor(ContextCompat.getColor(this, R.color.chart_axis_grid));
        rightAxis.setGridLineWidth(1f);
        rightAxis.setGranularity(0.00000001f);

        setData(mChartItems);

        mChart.animateX(900);

        subscribeTicker();

    }

    private void setData(List<ChartItem> chartItems) {

        final LineDataSet dataSet;

        ArrayList<Entry> values = new ArrayList<>();

        for (ChartItem item : chartItems) {

            values.add(new Entry(values.size(), (float) item.getValue()));

        }

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {

            dataSet = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            dataSet.setValues(values);
            dataSet.notifyDataSetChanged();
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();

        } else {

            dataSet = new LineDataSet(values, "entries");
            dataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);
            dataSet.setColor(ContextCompat.getColor(this, R.color.green));
            dataSet.setLineWidth(2f);
            dataSet.setDrawCircleHole(false);
            dataSet.setDrawCircles(false);
            dataSet.setDrawHorizontalHighlightIndicator(true);
            dataSet.setDrawVerticalHighlightIndicator(true);

            dataSet.setDrawFilled(true);
            dataSet.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return mChart.getAxisLeft().getAxisMinimum();
                }
            });

            dataSet.setHighlightLineWidth(1f);

            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.bg_chart);
            dataSet.setFillDrawable(drawable);

            LineData data = new LineData(dataSet);
            data.setDrawValues(false);

            mChart.setData(data);
        }
    }

    @Override
    public void viewed(float x, TextView dateTextView, TextView valueTextView) {
        if (mChartItems != null && mChartItems.size() > (int) x) {
            ChartItem item = mChartItems.get((int) x);

            dateTextView.setText(DateUtils.getFullFormattedDate(item.getTime()));
            valueTextView.setText(TextUtils.getFormattedDouble(item.getValue()));

        }
    }

    private void subscribeTicker() {
        SocketManager.getInstance().subscribeTicker("BTCUSD", this);
    }

    @Override
    public void onReceiveValue(ChartItem item) {

        if (mChartItems != null) {

            item.setFormattedTime(DateUtils.getTimeFormattedDate(item.getTime()));
            mChartItems.add(item);
            setData(mChartItems);
            mChart.invalidate();
        }

    }

    @Override
    public void connected() {
        subscribeTicker();
    }

    public class XAxisValueFormatter extends ValueFormatter {

        @Override
        public String getFormattedValue(float value) {

            if (value > 0
                    && mChartItems != null
                    && mChartItems.size() > (int) value) {
                return mChartItems.get((int) value).getFormattedTime();
            }

            return "";
        }

    }
}
