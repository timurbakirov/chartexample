package com.chart.example.screen;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.widget.TextView;

import com.chart.example.R;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

public class BubbleMarkerView extends MarkerView {

    private TextView mDateTextView;
    private TextView mValueTextView;
    private MarkerListener mMarkerListener;

    public BubbleMarkerView(Context context, int layoutResource, MarkerListener markerListener) {
        super(context, layoutResource);

        mMarkerListener = markerListener;
        mDateTextView = findViewById(R.id.date_text_view);
        mValueTextView = findViewById(R.id.value_text_view);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        mMarkerListener.viewed(e.getX(), mDateTextView, mValueTextView);

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2f), -getHeight() - getHeight() / 2);
    }

    @Override
    public void draw(Canvas canvas, float posX, float posY) {

        MPPointF offset = getOffsetForDrawingAtPoint(posX, posY);

        int saveId = canvas.save();
        canvas.translate(posX + offset.x, 0);
        draw(canvas);
        canvas.restoreToCount(saveId);
    }

    interface MarkerListener {

        void viewed(float x, TextView dateTextView, TextView valueTextView);

    }
}
