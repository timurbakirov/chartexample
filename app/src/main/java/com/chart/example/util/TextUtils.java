package com.chart.example.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class TextUtils {

    public static String getFormattedDouble(double value) {
        return getGroupFormat("###,###,###,###.########").format(value);
    }

    private static DecimalFormat getGroupFormat(String format) {
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(Locale.getDefault());
        formatSymbols.setDecimalSeparator('.');
        formatSymbols.setGroupingSeparator(' ');
        return new DecimalFormat(format, formatSymbols);
    }
}
