package com.chart.example.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final String FORMAT_FULL_DATE = "dd.MM.yyyy HH:mm:ss";
    private static final String FORMAT_TIME = "HH:mm:ss";


    public static String getFullFormattedDate(long timeInMillis) {
        return getFormattedDate(timeInMillis, FORMAT_FULL_DATE);
    }

    public static String getTimeFormattedDate(long timeInMillis) {
        return getFormattedDate(timeInMillis, FORMAT_TIME);
    }

    private static String getFormattedDate(long timeInMillis, String dateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.getDefault());

        Date date = new Date(timeInMillis);

        return simpleDateFormat.format(date);
    }

}
