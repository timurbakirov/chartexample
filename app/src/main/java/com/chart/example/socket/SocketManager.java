package com.chart.example.socket;

import android.util.Log;

import com.chart.example.model.ChartItem;
import com.chart.example.model.MessageModel;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;

public class SocketManager {

    private static final String WS_CHANNEL = "wss://api.bitfinex.com/ws/";


    private static SocketManager mSocketManager;

    private WebSocketClient mWebSocketClient;
    private TickerCallback mTickerCallback;
    private boolean mSubscribed = false;

    public static SocketManager getInstance() {
        if (mSocketManager == null) {
            mSocketManager = new SocketManager();
        }
        return mSocketManager;
    }

    public void connect() {
        if (mWebSocketClient != null) {
            close();
        }

        try {
            mWebSocketClient = new WebSocketClient(new URI(WS_CHANNEL)) {
                @Override
                public void onOpen(ServerHandshake handshakedata) {
                    Log.d("WebSocketClient", "onOpen");

                    if (mTickerCallback != null) {
                        mTickerCallback.connected();
                    }

                }

                @Override
                public void onMessage(String message) {
                    Log.d("WebSocketClient", "onMessage");
                    Log.d("WebSocketClient", message);

                    if (mSubscribed) {

                        try {
                            JSONArray jsonArray = new JSONArray(message);

                            if (jsonArray.length() == 11) {
                                if (mTickerCallback != null) {

                                    double value = jsonArray.getDouble(7);
                                    long time = Calendar.getInstance().getTimeInMillis();

                                    ChartItem chartItem = new ChartItem(time, value);

                                    mTickerCallback.onReceiveValue(chartItem);
                                }
                            }

                            return;
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("WebSocketClient", "message json array error");
                        }
                    }

                    try {
                        JSONObject jsonObject = new JSONObject(message);

                        if (jsonObject.getString("event").equals("subscribed")) {
                            mSubscribed = true;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("WebSocketClient", "message json object error");
                    }


                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    Log.d("WebSocketClient", "onClose");
                }

                @Override
                public void onError(Exception ex) {
                    Log.d("WebSocketClient", "onError");
                }

            };

            mWebSocketClient.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (mWebSocketClient != null) {
            mSubscribed = false;
            mWebSocketClient.close();
            mWebSocketClient = null;
        }
    }

    public void subscribeTicker(String pair, TickerCallback tickerCallback) {

        if (mSubscribed) {
            return;
        }

        mTickerCallback = tickerCallback;

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("event", "subscribe");
            jsonObject.put("channel", "ticker");
            jsonObject.put("pair", pair);

            subscribe(jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void subscribe(String message) {

        if (mWebSocketClient != null
                && mWebSocketClient.isOpen()) {
            mWebSocketClient.send(message);
        }

    }

    public interface TickerCallback {

        void connected();

        void onReceiveValue(ChartItem item);

    }

}
