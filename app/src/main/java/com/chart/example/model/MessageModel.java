package com.chart.example.model;

public class MessageModel {

    private String mEvent;

    private String mChannel;

    private String mPair;

    private String mChanId;

    public MessageModel(String mEvent, String mChannel, String mPair) {
        this.mEvent = mEvent;
        this.mChannel = mChannel;
        this.mPair = mPair;
    }

    public String getEvent() {
        return mEvent;
    }

    public String getChannel() {
        return mChannel;
    }

    public String getPair() {
        return mPair;
    }

    public String getChanId() {
        return mChanId;
    }

}