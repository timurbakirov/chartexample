package com.chart.example.model;

public class ChartItem {

    private long mTime;

    private String mFormattedTime;

    private double mValue;

    public ChartItem(long time, double value) {
        mTime = time;
        mValue = value;
    }

    public long getTime() {
        return mTime;
    }

    public double getValue() {
        return mValue;
    }

    public String getFormattedTime() {
        return mFormattedTime;
    }

    public void setFormattedTime(String formattedTime) {
        mFormattedTime = formattedTime;
    }
}
