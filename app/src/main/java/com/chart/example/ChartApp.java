package com.chart.example;

import android.app.Application;

import com.chart.example.socket.SocketManager;

public class ChartApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        SocketManager.getInstance().connect();

    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        SocketManager.getInstance().close();

    }
}
